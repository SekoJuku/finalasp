package kz.edu.astanait.dto.requestDto;

import lombok.Data;

@Data
public class AuthRequestDto {
    private String login;
    private String password;
    private String token;
    private String name;
}
