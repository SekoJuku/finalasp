package kz.edu.astanait.dto.requestDto;

import lombok.Data;

@Data
public class MessageRequestDto {
    private Long id;
    private Long chatId;
    private Long userId;
    private String text;
}
