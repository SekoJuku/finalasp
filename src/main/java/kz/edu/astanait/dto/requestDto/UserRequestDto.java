package kz.edu.astanait.dto.requestDto;

import lombok.Data;

@Data
public class UserRequestDto {
    private Long id;
    private String name;
}
