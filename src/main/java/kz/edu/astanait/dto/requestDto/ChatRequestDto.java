package kz.edu.astanait.dto.requestDto;

import lombok.Data;

@Data
public class ChatRequestDto {
    private Long id;
    private String name;
}
