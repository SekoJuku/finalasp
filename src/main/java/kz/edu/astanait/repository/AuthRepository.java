package kz.edu.astanait.repository;

import kz.edu.astanait.entity.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<Auth,Long> {
    boolean existsByLoginAndPassword(String login, String password);
    Auth findAuthByLoginAndPassword(String login, String password);
    boolean existsByLogin(String login);
    Auth findAuthByToken(String token);
}
