package kz.edu.astanait.service;

import kz.edu.astanait.dto.requestDto.UserRequestDto;
import kz.edu.astanait.entity.User;
import kz.edu.astanait.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;


    public User add(UserRequestDto requestDto) {
        User newUser = new User();
        newUser.setName(requestDto.getName());
        return userRepository.save(newUser);
    }

    public ResponseEntity<?> createUser(UserRequestDto requestDto) {
        return ResponseEntity.ok(add(requestDto));
    }
    public ResponseEntity<?> editUser(UserRequestDto requestDto) {
        return ResponseEntity.ok(add(requestDto));
    }
    public ResponseEntity<?> deleteUser(UserRequestDto requestDto) {
        userRepository.deleteById(requestDto.getId());
        return ResponseEntity.ok("User is deleted");
    }


    public User findById(Long userId) {
        return userRepository.getById(userId);
    }
}
