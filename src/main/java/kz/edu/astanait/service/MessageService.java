package kz.edu.astanait.service;

import kz.edu.astanait.dto.requestDto.MessageRequestDto;
import kz.edu.astanait.entity.Message;
import kz.edu.astanait.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MessageService {
    private MessageRepository messageRepository;

    public ResponseEntity<?> createMessage(MessageRequestDto requestDto) {
        Message newMessage = new Message();
        newMessage.setUserId(requestDto.getUserId());
        newMessage.setChatId(requestDto.getChatId());
        newMessage.setText(requestDto.getText());
        return ResponseEntity.ok(messageRepository.save(newMessage));
    }

    public ResponseEntity<?> editMessage(MessageRequestDto requestDto) {
        Message newMessage = new Message();
        newMessage.setUserId(requestDto.getUserId());
        newMessage.setChatId(requestDto.getChatId());
        newMessage.setText(requestDto.getText());
        return ResponseEntity.ok(messageRepository.save(newMessage));
    }

    public ResponseEntity<?> deleteMessage(MessageRequestDto requestDto) {
        messageRepository.deleteById(requestDto.getId());
        return ResponseEntity.ok("Message is deleted!");
    }
}
