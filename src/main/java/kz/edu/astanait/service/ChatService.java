package kz.edu.astanait.service;

import kz.edu.astanait.dto.requestDto.ChatRequestDto;
import kz.edu.astanait.entity.Chat;
import kz.edu.astanait.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ChatService {
    private ChatRepository chatRepository;

    public ResponseEntity<?> createChat(ChatRequestDto requestDto) {
        Chat newChat = new Chat();
        newChat.setName(requestDto.getName());
        return ResponseEntity.ok(chatRepository.save(newChat));
    }

    public ResponseEntity<?> editChat(ChatRequestDto requestDto) {
        Chat newChat = new Chat();
        newChat.setName(requestDto.getName());
        return ResponseEntity.ok(chatRepository.save(newChat));
    }
    public ResponseEntity<?> deleteChat(ChatRequestDto requestDto) {
        chatRepository.deleteById(requestDto.getId());
        return ResponseEntity.ok("Chat is deleted");
    }
}
