package kz.edu.astanait.service;

import kz.edu.astanait.dto.requestDto.AuthRequestDto;
import kz.edu.astanait.dto.requestDto.UserRequestDto;
import kz.edu.astanait.entity.Auth;
import kz.edu.astanait.entity.User;
import kz.edu.astanait.repository.AuthRepository;
import kz.edu.astanait.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final UserService userService;

    public ResponseEntity<?> login(AuthRequestDto requestDto) {
        Auth auth = authRepository.findAuthByLoginAndPassword(requestDto.getLogin(), requestDto.getPassword());
        if(auth != null) {
            auth.setToken(UUID.randomUUID().toString());
            authRepository.save(auth);
            return ResponseEntity.ok(auth.getToken());
        } else {
            return ResponseEntity.badRequest().body("Invalid login or password!");
        }
    }


    public ResponseEntity<?> register(AuthRequestDto requestDto) {
        if(authRepository.existsByLogin(requestDto.getLogin()))
            return ResponseEntity.badRequest().body("Such login already exists!");
        UserRequestDto request = new UserRequestDto();
        request.setName(requestDto.getName());
        User newUser = userService.add(request);

        Auth newAuth = new Auth();
        newAuth.setLogin(requestDto.getLogin());
        newAuth.setPassword(requestDto.getPassword());
        newAuth.setUserId(newUser.getId());
        newAuth.setToken(UUID.randomUUID().toString());
        authRepository.save(newAuth);
        return ResponseEntity.ok(newUser);
    }

    public ResponseEntity<?> checkToken(String token) {
        Auth auth = authRepository.findAuthByToken(token);
        if(auth == null)
            return ResponseEntity.badRequest().body("Can not find token");
        User user = userService.findById(auth.getUserId());
        return ResponseEntity.ok(user);
    }

    public boolean check(String token) {
        String url = "http://localhost:8080/api/auth/check";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<User> result = restTemplate.getForEntity(url, User.class);
        User user = result.getBody();
        if (user != null)
            return true;
        return false;
    }
}
