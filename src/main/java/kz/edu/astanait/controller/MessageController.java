package kz.edu.astanait.controller;

import kz.edu.astanait.dto.requestDto.MessageRequestDto;
import kz.edu.astanait.service.AuthService;
import kz.edu.astanait.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/message")
public class MessageController {
    private final MessageService messageService;
    private final AuthService authService;

    @PostMapping("/create")
    public ResponseEntity<?> createMessage (@RequestHeader(name = "Authorization") String token, @RequestBody MessageRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return messageService.createMessage(requestDto);
    }
    @PostMapping("/edit")
    public ResponseEntity<?> editMessage(@RequestHeader(name = "Authorization") String token, @RequestBody MessageRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return messageService.editMessage(requestDto);
    }
    @PostMapping("/delete")
    public ResponseEntity<?> deleteMessage(@RequestHeader(name = "Authorization") String token, @RequestBody MessageRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return messageService.deleteMessage(requestDto);
    }
}
