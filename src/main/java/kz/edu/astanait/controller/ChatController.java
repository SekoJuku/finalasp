package kz.edu.astanait.controller;

import kz.edu.astanait.dto.requestDto.ChatRequestDto;
import kz.edu.astanait.service.AuthService;
import kz.edu.astanait.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/chat")
public class ChatController {
    private final ChatService chatService;
    private final AuthService authService;

    @PostMapping("/create")
    public ResponseEntity<?> createChat(@RequestHeader(name = "Authorization") String token, @RequestBody ChatRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return chatService.createChat(requestDto);
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editChat(@RequestHeader(name = "Authorization") String token, @RequestBody ChatRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return chatService.editChat(requestDto);
    }
    @PostMapping("/delete")
    public ResponseEntity<?> deleteChat(@RequestHeader(name = "Authorization") String token, @RequestBody ChatRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return chatService.deleteChat(requestDto);
    }
}
