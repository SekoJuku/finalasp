package kz.edu.astanait.controller;

import kz.edu.astanait.dto.requestDto.UserRequestDto;
import kz.edu.astanait.service.AuthService;
import kz.edu.astanait.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/user")
public class UserController {
    private final UserService userService;
    private final AuthService authService;

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestHeader(name = "Authorization") String token, @RequestBody UserRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return userService.createUser(requestDto);
    }
    @PostMapping("/edit")
    public ResponseEntity<?> editUser(@RequestHeader(name = "Authorization") String token, @RequestBody UserRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return userService.editUser(requestDto);
    }
    @PostMapping("/delete")
    public ResponseEntity<?> deleteUser(@RequestHeader(name = "Authorization") String token, @RequestBody UserRequestDto requestDto) {
        if(!authService.check(token))
            return ResponseEntity.badRequest().body("Something wrong with token!");
        return userService.deleteUser(requestDto);
    }

}
