package kz.edu.astanait.controller;

import kz.edu.astanait.dto.requestDto.AuthRequestDto;
import kz.edu.astanait.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthService authService;


    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthRequestDto requestDto) {
        return authService.login(requestDto);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody AuthRequestDto requestDto) {
        return authService.register(requestDto);
    }

    @GetMapping("/check")
    public ResponseEntity<?> checkToken(@RequestHeader(name = "token") String token) {
        return authService.checkToken(token);
    }
}
